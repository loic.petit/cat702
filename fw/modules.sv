// Code your design here
`timescale 1ns/1ps
module lshift(input [7:0] I, output [7:0] Q);
    genvar i;
    generate
        for (i = 0; i < 7; i++) begin : shift
            assign Q[i+1] = I[i];
        end
    endgenerate
    assign Q[0] = I[7] ^ I[6];
endmodule

module keyshift(input [7:0] I[8], output [7:0] Q[8]);
    genvar i;
    generate
        for (i = 0; i < 7; i++) begin : shift
            lshift l(I[i == 0 ? 7:(i-1)], Q[i]);
        end
    endgenerate
    lshift l7(I[7] ^ I[6], Q[7]);
endmodule

module sbox(
    input [7:0] I,
    output [7:0] Q,
    input [7:0] key[8]
);
    genvar i, j;
    generate
        for (i = 0; i < 8; i++) begin : xorblock
            wire [7:0] x;
            for (j = 0; j < 8; j++) begin : andblock
                assign x[j] = key[j] [i] & I[j];
            end
            assign Q[i] = x[0] ^ x[1] ^ x[2] ^ x[3] ^ x[4] ^ x[5] ^ x[6] ^ x[7];
        end
    endgenerate
endmodule

module mux81(output out, input [7:0] D, input [2:0] S);
    wire [7:0] internal;
    genvar i;
    generate
        for (i = 0; i < 8; i++) begin : shift
            assign internal[i] = D[i] & (S == i);
        end
    endgenerate
    assign out = internal[0] | internal[1] | internal[2] | internal[3] | internal[4] | internal[5] | internal[6] | internal[7];

endmodule

module mux28(input sel, input [7:0] i1, input [7:0] i2, output [7:0] out);
    genvar i;
    generate
        for (i = 0; i < 8; i++) begin
            assign out[i] = i1[i] & (!sel) | i2[i] & sel;
        end
    endgenerate
endmodule

module cat702_core(input data, input clk, input select, output out, input [7:0] s1_transform[8], input [7:0] s2_transform[8], output [7:0] state_out);
    reg [2:0] b;
    reg [7:0] state;
    reg [7:0] transform [8];

    wire [7:0] keyshift_out[8];
    wire [7:0] s1_out;
    wire [7:0] s1_in;
    wire [7:0] s2_out;
    keyshift ks(transform, keyshift_out);

    mux28 s1_in_mux(data, s2_out, state, s1_in);
    sbox s1(s1_in, s1_out, s1_transform);
    sbox s2(state, s2_out, transform);
    mux81 mux(out, state, b);
    assign state_out = state;
    always @(posedge select or posedge clk) begin
        if (select == 1) begin
            state <= 8'hAB;
            b <= 0;
        end
        else begin
            if (b == 7) begin
                state <= s1_out;
            end
            else begin
                if (!data) state <= s2_out;
            end
            b <= b+1'b1;
        end
    end

    always @(posedge select or negedge clk) begin
        if (select == 1) begin
            transform <= s2_transform;
        end
        else begin
            if (b == 0)
                transform <= s2_transform;
            else
                transform <= keyshift_out;
        end
    end
endmodule

module keyrom(input [5:0] keyselect, output [7:0] s2_transform[8]);
    wire [63:0] data_out;
    rom data(keyselect, data_out);

    assign s2_transform[7] = data_out[7:0];
    assign s2_transform[6] = data_out[15:8];
    assign s2_transform[5] = data_out[23:16];
    assign s2_transform[4] = data_out[31:24];
    assign s2_transform[3] = data_out[39:32];
    assign s2_transform[2] = data_out[47:40];
    assign s2_transform[1] = data_out[55:48];
    assign s2_transform[0] = data_out[63:56];

    //assign s2_transform = { 8'hE0, 8'hF2, 8'h70, 8'h81, 8'hC1, 8'h3C, 8'h04, 8'hF8 };
    //assign s2_transform = { 8'hf0, 8'h20, 8'he1, 8'h81, 8'h7c, 8'h04, 8'hfa, 8'h02 };
endmodule

module cat702(input data, input clk, input select, output out, input [5:0] keyselect);
    wire [7:0] s2[8];
    wire [7:0] s1[8];
    wire [7:0] state_out;
    keyrom key(keyselect, s2);
    assign s1 = {8'hFF, 8'hFE, 8'hFC, 8'hF8, 8'hF0, 8'hE0, 8'hC0, 8'h7F};
    cat702_core core(data, clk, !!select, out, s1, s2, state_out);
endmodule