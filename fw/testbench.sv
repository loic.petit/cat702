`timescale 1ns/1ps
module test;
    reg [7:0] s1_transform [8];
    wire [7:0] s2_transform[8];
    reg data, clk, select;
    wire out;
    wire [7:0] state_out;
    reg [5:0] keyselect;


    cat702 cp08(data, clk, select, out, keyselect);
    keyrom asd(keyselect, s2_transform);
    initial begin
        byte r, f_char;
        int f, fr;
        int total;
        f = $fopen("..\\..\\sw\\gen.txt", "r");
        fr = $fopen("..\\..\\sw\\sim-data.txt", "w");
        // Dump waves
        $dumpfile("dump.vcd");
        $dumpvars(1);

        keyselect = 6'hD;
        data = 0;
        clk = 0;
        select = 1;
        #100;
        clk = 1;
        #100;
        select = 0;
        #100;
        clk = 0;
        #100;

        for (int b = 0; b < 256; b++) begin
            r = 0;
            //$display("init [%02x]", state_out);
            for (int i = 0; i < 8; i++) begin
                data = (b & (1 << i)) != 0;
                #100;
                clk = 1;
                #100;
                clk = 0;
                #1000;
                r |= (out << i);
                $display("\t %d [%02x]", data, state_out);
            end
            //select = 1;
            //#100;
            //clk = 1;
            //#100;
            //select = 0;
            //#100;
            //clk = 0;
            //#100;

            $display("%02x => %02x", b, r);
        end

        select = 1;
        data = 0;
        for (int b = 0; b < 17000; b++) begin
            r = $fgetc(f);
            if (r == "S") select = 1;
            else if (r == "s") select = 0;
            else if (r == "C") clk = 1;
            else if (r == "c") clk = 0;
            else if (r == "D") data = 1;
            else if (r == "d") data = 0;
            else if (r == "#") break;
            else continue;
            #100;
            $fwrite(fr, "%d", out);
            //$display("%d", out);
            total++;
            if (total%128 == 0) $fwrite(fr, "\n");
        end
        $fclose(f);
        $fclose(fr);
    end
endmodule