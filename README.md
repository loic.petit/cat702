# CAT702 Reproduction Project

## Project summary
This project aims to create a CAT702 reproduction. This chip is available on Capcom Sony ZN systems and Taito G-Net boards.
This chip covers part of the security scheme of these systems and it is mandatory to have them running to convert a game.

## Warning
:warning: This project PCB has not been tested yet but a breadboard implementation works flawlessly.

Moreover, this is a fun and experimental project for me, so if you find mistakes or things that are not optimal please 
let me know.

## Hardware
This repro is based on the MachXO2-1200U FPGA in QFN32. However, this is possible to load the firmware in a MachXO2-256U
but the key selection must be hardcoded in the firmware.

Those chips are available by the [TinyFPGA Ax](https://tinyfpga.com/) project. Assuming you have some, you can create a
breadboard implementation without much issues. You just need to do level shifting (warning, the ZN2 board uses 4.7k 
and 1k pull down resistors).

| Function | Direction | Cat702 Pin | TinyFPGA Ax Pin | FPGA Pin |
| --- | --- | --- | --- | --- |
| Clock | In | 7 | 16 | 4 |
| Output | Out | 14 | 17 | 5 |
| Select | In | 5-6 | 19 | 9 |
| Data | In | 8 | 20 | 10 |

Key selection is done by using pull-down inputs on inputs on FPGA pins 11 through 17 (MSB first).

## Key selection

| Key Select | Chip |
| --- | --- |
| 000000 | ac01 |
| 000001 | ac02 |
| 000010 | tw01 |
| 000011 | tw02 |
| 000100 | at01 |
| 000101 | at02 |
| 000110 | cp01 |
| 000111 | cp02 |
| 001000 | cp03 |
| 001001 | cp04 |
| 001010 | cp05 |
| 001011 | cp06 |
| 001100 | cp07 |
| 001101 | cp08 |
| 001110 | cp09 |
| 001111 | cp10 |
| 010000 | cp11 |
| 010001 | cp12 |
| 010010 | cp13 |
| 010011 | et01 |
| 010100 | et02 |
| 010101 | et03 |
| 010110 | mg01 |
| 010111 | mg02 |
| 011000 | mg03 |
| 011001 | mg04 |
| 011010 | mg05 |
| 011011 | mg06 |
| 011100 | mg08 |
| 011101 | mg09 |
| 011110 | mg11 |
| 011111 | mg13 |
| 100000 | mg14 |
| 100001 | tt01 |
| 100010 | tt02 |
| 100011 | tt03 |
| 100100 | tt04 |
| 100101 | tt05 |
| 100110 | tt06 |
| 100111 | tt07 |
| 101000 | tt10 |
| 101001 | tt16 |
| 101010 | kn01 |
| 101011 | kn02 |

## License
This work is licensed under the [BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause) license.

If however you think about commercialising this as is, or if you consider integrating this in another project, the 
author would like to be notified.

## Author
This project was made by [Loïc *WydD* Petit](https://twitter.com/WydD).