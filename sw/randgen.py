import random

random.seed(42)

select = True
clk = False
data = False

print("S", end="")
for d in range(4096*4):
    if not select and random.randint(0, 99) < 25:
        select = not select
        print("S" if select else "s", end="")
        continue
    if select and random.randint(0, 99) < 25:
        select = not select
        print("S" if select else "s", end="")
        continue
    b = random.randint(0, 1)
    if b == 0:
        clk = not clk
        print("C" if clk else "c", end="")
        continue
    else:
        data = not data
        print("D" if data else "d", end="")
        continue
print("#", end="")