# CODE LARGELY INSPIRED BY MAME SOURCE
# https://github.com/mamedev/mame/blob/master/src/mame/machine/cat702.cpp
# Original code is under BSD-3-Clause license
# Original code copyright holders: smf

initial_sbox = [0xff, 0xfe, 0xfc, 0xf8, 0xf0, 0xe0, 0xc0, 0x7f]


def rshift(r):
    return ((r >> 1) & 0xff) | (((r << 7) & 0x80) ^ (r & 0x80))


def lshift(r):
    return ((r << 1) & 0xff) | (((r >> 7) ^ (r >> 6)) & 1)

class Cat702:
    def __init__(self, init_transform):
        self.select = 1
        self.clock = 1
        self.datain = 1
        self.transform = list(init_transform)
        self.init_transform = init_transform
        self.state = 0
        self.output = False
        self.bit = 0

    # Apply a sbox
    def apply_sbox(self, sbox):
        r = 0
        s = self.state
        test = [0] * 8
        for i in range(0, 8):
            #test[i] = s & sbox[i]
            t = -(s & 1)
            s = s >> 1
            r = t & (sbox[i] ^ r) | (~t) & r
            #-(s & (1 << i)) & 34
            #if self.state & (1 << i):
            #    r ^= sbox[i] & self.state

        self.state = r

    def write_select(self, _select):
        if self.select != _select:
            if not _select:
                self.state = 0xab
                self.bit = 0
                self.transform = list(self.init_transform)
            else:
                self.output = True

            self.select = _select

    def write_clock(self, _clock):
        if (not _clock) and self.clock and (not self.select):
            if self.bit == 0:
                # Apply the initial sbox
                self.transform = list(self.init_transform)
            else:
                self.transform = [
                    lshift(
                        self.transform[i-1] ^ self.transform[i]
                        if i == 7 else
                        self.transform[i-1]
                    )
                    for i in range(8)
                ]

        if _clock and (not self.clock) and (not self.select):
            if not self.datain:
                self.apply_sbox(self.transform)
            if self.bit == 7:
                self.apply_sbox(initial_sbox)

            self.bit += 1
            self.bit &= 7

            # Compute the output and change the state
            self.output = (((self.state >> self.bit) & 1) != 0)

        self.clock = _clock

    def write_datain(self, _datain):
        self.datain = _datain
"""
ff ==> 55
fe ==> ab
fd ==> 55
fc ==> 57
fb ==> 6d
fa ==> ab
f9 ==> 6d
f8 ==> af
f7 ==> b5
f6 ==> db
"""

cat702 = Cat702([
    0xe0, 0xf2, 0x70, 0x81, 0xc1, 0x3c, 0x04, 0xf8
])

cat702.write_clock(1)
cat702.write_select(0)
cat702.write_clock(0)

# noinspection PyInterpreter
for b in range(0, 256):
    r = 0
    # print("init [%02x]" % cat702.state)
    for i in range(0, 8):
        cat702.write_datain(b & (1 << i))
        cat702.write_clock(1)
        cat702.write_clock(0)
        r = r | ((1 if cat702.output else 0) << i)
        # print("\t%02x" % cat702.state)
    print("%02x" % b, "==>", "%02x" % r)

    cat702.write_select(1)
    cat702.write_clock(1)
    cat702.write_select(0)
    cat702.write_clock(0)
"""
cat702.write_select(1)
for b in range(0, 256):
    cat702.state = b
    cat702.apply_sbox(initial_sbox)
    print("%02x" % b, "==>", "%02x" % cat702.state)
        """
"""
print(" ", end="\t")
for s in range(0, 8):
    print("  ", s, "   ", end="\t")
print()
coefs = [list(cat702.transform) for i in range(8)]
transform = cat702.init_transform
print("%02x %02x %02x %02x %02x %02x %02x %02x" % (transform[0], transform[1], transform[2], transform[3], transform[4], transform[5], transform[6], transform[7]))
for sel in range(1, 8):
    base = coefs[sel - 1]
    coefs[sel] = [
        lshift(base[i] ^ base[i-1])
        if i == 7 else
        lshift(base[i-1])
        for i in range(8)
    ]
    transform = coefs[sel]
    print("%02x %02x %02x %02x %02x %02x %02x %02x" % (transform[0], transform[1], transform[2], transform[3], transform[4], transform[5], transform[6], transform[7]))
"""
"""
for bit in range(0, 8):
    print(bit, end="\t")
    for sel in range(0, 8):
        res = cat702.compute_sbox_coef(sel, bit)
        # for _ in range(0, sel):
        #     res = rshift(res)
        print(format(res, 'b').zfill(8), end="\t")
    print()

    print(" ", end="\t")
    for sel in range(0, 8):
        res = coefs[sel][bit]
        # for _ in range(0, sel):
        #     res = rshift(res)
        print(format(res, 'b').zfill(8), end="\t")

    print()
    print()
"""