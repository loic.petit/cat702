data = [int(r) for r in open("data.txt", "r").read() if r == '0' or r == '1']
sim = [int(r) for r in open("sim-data.txt", "r").read() if r == '0' or r == '1']
gen = [r for r in open("gen.txt", "r").read() if r != "\n" and r != "\r"]

x = 0
fails = 0
dat = False
sel = True
clk = False
for i in range(len(data)):
    # if gen[i] == 's':
    #     print("s encountered @", i)
    #     print("".join(gen[max(i-10, 0):i])+"["+gen[i]+"]"+"".join(gen[i+1:i+10]))
    #     print("%s[%d]%s" % ("".join([str(a) for a in data[max(i-10, 0):i]]), data[i], "".join([str(a) for a in data[i+1:i+10]])))
    if gen[i] == "s":
        x = 0
        sel = False
        print("Reset")
    elif gen[i] == "C":
        x = (x+1) % 8
        clk = True
    elif gen[i] == "c":
        clk = False
    elif gen[i] == "S":
        sel = True
    elif gen[i] == "d":
        dat = False
    elif gen[i] == "D":
        dat = True

    if data[i] != sim[i]:
        print(i+1, "[dat=%d, clk=%d, sel=%d, b=%d, o=%d]" % (dat, clk, sel, x, data[i]), "ER" if data[i] != sim[i] else "OK", "".join(gen[max(i-10, 0):i])+"["+gen[i]+"]"+"".join(gen[i+1:i+10]))
        fails += 1

print("Total fails:", fails)

