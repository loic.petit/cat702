# CAT702 Testing lab

## Contents
This directory contains mainly experimental code to figure out how the cat702 works.

* `cat702.[cpp,py]` contains the code LARGELY inspired from MAME source with a bunch of cleaning and adaptation to simplify the implementation
* `gen*.txt` Testing data. c/C means clock (0/1), d/D means data (0/1), s/S means select (0/1)
* `data*.txt` The output of each generated data, extracted from the actual chip
* `out_*.txt` Byte-level output of the chip measured on the actual chip.
* `randgen.py` Generator of gen files
* `test-sim-data.py` Verify that the fpga simulation is the same as the chip data
* `cat702_arduino` Arduino code to do the data extraction

As stated in the source files, the code from MAME is under BSD 3 license.