// CODE LARGELY INSPIRED IF NOT COPIED BY MAME SOURCE
// https://github.com/mamedev/mame/blob/master/src/mame/machine/cat702.cpp
// Original code is under BSD-3-Clause license
// Original code copyright holders: smf

#include <stdio.h>
#include <string.h>
#include <stdint.h>

class cat702_device {
public:
    cat702_device(const uint8_t transform[8]);

    void write_select(bool state);

    void write_datain(bool state);

    void write_clock(bool state);


    bool m_output;
private:
    uint8_t compute_sbox_coef(int sel, int bit);

    void apply_sbox(const uint8_t *sbox);

    uint8_t m_init_transform[8];
    uint8_t m_transform[8];

    int m_select;
    int m_clock;
    int m_datain;
    uint8_t m_state;
    uint8_t m_bit;
};

inline uint8_t lshift(uint8_t r) {
    return ((r << 1) & 0xff) | (((r >> 7) ^ (r >> 6)) & 1);
}

cat702_device::cat702_device(const uint8_t transform[8]) :
        m_select(1),
        m_clock(1),
        m_datain(1) {
    memcpy(m_init_transform, transform, sizeof(m_transform));
}

// Apply a sbox
void cat702_device::apply_sbox(const uint8_t *sbox) {
    int i;
    uint8_t r = 0;
    for (i = 0; i < 8; i++)
        if (m_state & (1 << i))
            r ^= sbox[i];

    m_state = r;
}

void cat702_device::write_select(bool state) {
    if (m_select != state) {
        if (!state) {
            m_state = 0xab;
            m_bit = 0;
            memcpy(m_transform, m_init_transform, sizeof(m_transform));
        } else {
            m_output = (1);
        }

        m_select = state;
    }
}

static const uint8_t initial_sbox[8] = {0xff, 0xfe, 0xfc, 0xf8, 0xf0, 0xe0, 0xc0, 0x7f};

void cat702_device::write_clock(bool state) {

    if (!state && m_clock && !m_select) {
        if (m_bit == 0) {
            memcpy(m_transform, m_init_transform, sizeof(m_transform));
        } else {
            uint8_t transform7 = m_transform[7];
            m_transform[7] = lshift(m_transform[6] ^ transform7);
            m_transform[6] = lshift(m_transform[5]);
            m_transform[5] = lshift(m_transform[4]);
            m_transform[4] = lshift(m_transform[3]);
            m_transform[3] = lshift(m_transform[2]);
            m_transform[2] = lshift(m_transform[1]);
            m_transform[1] = lshift(m_transform[0]);
            m_transform[0] = lshift(transform7);
        }
    }

    if (state && !m_clock && !m_select) {
        if (!m_datain)
            apply_sbox(m_transform);
        if (m_bit == 7)
            // Apply the initial sbox
            apply_sbox(initial_sbox);

        m_bit++;
        m_bit &= 7;
        // Compute the output and change the state
        m_output = (((m_state >> m_bit) & 1) != 0);
    }

    m_clock = state;
}

void cat702_device::write_datain(bool state) {
    m_datain = state;
}

#define HIGH 0x1
#define LOW 0x0


int main(int argc, char *argv) {
    uint8_t transform[8] = {0xe0, 0xf2, 0x70, 0x81, 0xc1, 0x3c, 0x04, 0xf8};
    cat702_device cat702 = cat702_device(transform);

    cat702.write_clock(HIGH);
    cat702.write_select(LOW);
    cat702.write_clock(LOW);

    for (int b = 0; b < 256; b++) {
        uint8_t r = 0;
        for (int i = 0; i < 8; i++) {
            cat702.write_datain((b & (1 << i)) ? HIGH : LOW);
            cat702.write_clock(HIGH);
            cat702.write_clock(LOW);
            r = r | (cat702.m_output << i);
        }
        printf("%02x ==> %02x\n", b, r);

        cat702.write_select(HIGH);
        cat702.write_select(LOW);
    }
    cat702.write_select(HIGH);
}
